# Orte
- albina_orte_p: Orte (zu erweitern)
- capital_cities: Hauptstaedte
- towns_zeichen: Staedte, deren Label in kyrillischen Zeichen und in der englischen Uebersetzung angezeigt werden
- verbauung_a: grosse Staedte (europaweit)

## albina_orte_p erweitern
- Shape-Files gis_osm_places_free_1 (Geofabrik)
- Attribut fclass. city, town (manuell wählen)
- style, category, level_x über Größe der Orte wählen (anhand Größe und Erscheinen bei Zoomstufen auf GoogleMaps/Outdooractive, Attribut "Population"); l_ext_o, l_inn_o geben die Position des Labels abhängig vom Symbol an; TRE_levelx: 0 (nur für Trentino 1)