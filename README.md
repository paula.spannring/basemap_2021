# Basemap Geodaten
- DEM: no Upload in GitLab
- boundaries: provinces, countries
- gondola: lifts EUREGIO
- label
- mask_TMS: masks for creating TMS
- peaks
- roads
- towns
- waters: ocean, river, lakes
- create_TMS: Files and Help for Creating TMS in webp-format

## Source
- DEM: https://land.copernicus.eu/ , https://data.opendataportal.at/dataset/dtm-europe, https://earthexplorer.usgs.gov/
- Shape-Files: https://www.openstreetmap.org , https://download.geofabrik.de/ , http://overpass-turbo.eu/

