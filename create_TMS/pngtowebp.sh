#!/bin/bash
$initpath = Get-Location
# get all directories
foreach($path in Get-Childitem) {
     if ($path.Attributes -eq "Directory") {
         set-location $path.FullName
           # get all files in the directory
           $images = Get-ChildItem -recurse $dir

           # loop through every image
           foreach ($img in $images) {
             # output file will be written in the same directory
             # but with .webp extension instead of old extension
             $outputName = $img.DirectoryName + "\" + $img.BaseName + ".webp"

             # since you have the cwebp bin folder in PATH just type the command
             # more options https://developers.google.com/speed/webp/docs/cwebp
             D:\GitLab\base-map\create_TMS\libwebp-1.0.2-windows-x64\bin.\cwebp -q 100 $img.FullName -o $outputName
           }
     }
 }
 set-location $initpath