
# TMS in webp-Format erzeugen

## TMS erzeugen aus QGIS
- in QGIS: Erweiterungen > QMetaTiles > QMetaTiles
- Pfad für Output eingeben
- Layer extent: Polygon für Maske/Rahmen wählen

## TMS von png zu webp konvertieren
- in Power Shell:
- Ordner wählen, in dem die TMS gespeichert sind, mit cd -Pfad-
- aus pngtowebp.sh (Skript mit Editor öffnen) Code kopieren, einfügen
-> neben den png-Dateien, werden die gleichen webp-Dateien gespeichert
		
-> wichtig: Pfad zu cwebp-Datei muss richtig sein (in bin-Ordner)